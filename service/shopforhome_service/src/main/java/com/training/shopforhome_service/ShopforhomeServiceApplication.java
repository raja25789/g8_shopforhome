package com.training.shopforhome_service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShopforhomeServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShopforhomeServiceApplication.class, args);
	}

}
